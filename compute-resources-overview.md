# BRC Compute Resources 



This page attempts to give an overview of the local and national cyberinfrastructure compute resources that Berkeley Research Computing (BRC) provides or facilitates access to. Local compute resources include Savio and the Analytics Environments on Demand (AEod). National compute resources include the NSF-funded XSEDE resources, that are all *freely available to UC Berkeley affiliated researchers*, and commercial Cloud Prodivers, namely Amazon's AWS, Google's GCP, and Microsoft's Azzure. There is significant overlap in the use cases of these compute resources. 

| Resource  | HPC or VM | GPU | Cost      | Ease of Acccess     | 
|:---------:|:---------:|:---:|:---------:|:-------------------:|
| Savio     | HPC       | Y   | Free      | 10 business days    | 
| AEoD      | VM        | Y   | use-case  | Consultation needed |
| Jetstream | VM        | N   | Free      | 24 hrs turn-around  |  
| Bridges   | HPC       | Y   | Free      | 24 hrs turn-around  |  
| Comet     | HPC       | Y   | Free      | 24 hrs turn-around  |  
| Stampede2 | HPC       | Y   | Free      | 24 hrs turn-around  | 
| AWS       | VM        | Y   | Commercial|    10 business days | 
| GCP       | VM        | Y   | Commercial|    10 business days | 
| AZURE     | VM        | Y   | Commercial|    10 business days | 

**Note: XSEDE resources are generally available within the same day** as creating a user account on their site and having one of the BRC consultants add you to the *XSEDE Campus Champion Allocation*. 

**Note: General Workflow Strategies will be coded as a vertical accordion** HTML / CSS structure in the final documentation.

## General Workflow Strategies

Although finding the optimal compute resource solution(s) is often a case-by-case endeavor, there are several general workflow strategies based on user needs. 

#### 1. Batch job submission (Command Line / Terminal)

The most typical or conventional high performance computing (HPC) use case is command line *(or Terminal)* batch job submissions. These are performed by writing jobs in a batch file that uses linux resource manager commands, such as **SLURM** or **PBS**, to execute your code on a linux HPC cluster. 

**SLURM: Simple Linux Utility Resource Manager** | this is the most widely used resource manager lanaguage and is used on **Savio, Comet, Bridges, and Stampede2**. Our documentation is based on the use of SLURM.

#### 2. GPU-intensive and VM use-cases 

AI and Deep Learning require intensive use of GPUs for massive parallel computing. There are several compute resources that are aimed at GPU-intensive workloads. 

**...more to come...**

#### 3. OS specific workflows 

Windows specific...

Linux OS...