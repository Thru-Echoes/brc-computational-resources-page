# This was a fully awarded Startup Allocation request  

## XSEDE Startup Allocation Request


**PI:** <THE-RESEARCHER>
**Allocation Manager:** <THE-RESEARCHER>         # PI and Allocation Manager are almost always same person (rare cases when not)


**PI’s CV:** to be attached (NSF/NIH format, 2-page limit)    # Through XSEDE User Portal 


**Title:** Sensor Analysis for Precision Agriculture
**Primary field of Science:** Electrical Engineering


**Keywords:** magnetic nanodots, magnetic anisotropy, MEMS, sensors, machine learning


**Resource Requested:** Jetstream (IU/TACC)
**Resource Requested Amount:** 50,000 service units


**Resource Requested:** Jetstream Storage
**Resource Requested Amount:** 100GB


**Resource Requested:** Comet (SDSC Dell Cluster with Intel Haswell Processors)
**Resource Requested Amount:** 50,000 service units
 
**Resource Requested:** Data Oasis (SDSC)
**Resource Requested Amount:** 100GB


**Research Statement:**

    This research project aims to analyze both device physics and sensing data relevant to precision agricultural applications in order to inform the development of better sensors. In one aspect of the project, the magnetic material behavior in nanoscale sensors and devices will be explored using the Object Oriented MicroMagnetic Framework (OOMMF) software installed on the Jetstream cluster. We have already utilized almost all of our resources in a trial allocation to do such work, which involved running micromagnetic simulations in OOMMF through a Jupyter notebook and determining the magnetization behavior in the device based on design and material constraints.


    In the other aspect of this project, we will analyze large amounts of sensor data from agricultural crops of interest in order to execute machine learning algorithms and identify the key features in the data that indicate crop quality. Initial data will come from infrared photodetectors and other commercial sensors. However, once the principle component analysis is complete and we have identified the important wavelengths relevant to the crop quality, this information will be fed back into our own development of new sensors and magnetic devices for crop analysis and precision agriculture.


**Use of Computational Resources:**

    The computing clusters will be used for both material and device simulations as well as machine learning and sensing data analysis. We have performed data exploration and simulation benchmarking with OOMMF through a Jupyter notebook image developed and ran on Jetstream. Once we are able to scale our simulations to the final level in future iterations for modeling of complex magnetic interactions in nanoscale devices and structures, CPU and RAM requirements will likely scale non-linearly. For benchmarking purposes, sample simulations of 44 CPUs and 120 GB of RAM took 8-12 hours of computation time.

    We would also like to analyze agricultural sensing data on the Comet server using machine learning for determination of the principle components relevant to the quality of the crop in question. We will have sensor data across a large number of wavelengths, numerous scans, and countless crop samples of various quality. Due to the large data set and also the numerous components involved, these types of analyses will likely require significant amounts of RAM. However, since the analysis does not require a graphic user interface (unlike OOMMF on Jupyter notebook), the commands can be submitted through a command-like interface.


This allocation request was prepared in collaboration with the UC Berkeley Campus Champion.


**Related Grant Information:**

Not applicable.