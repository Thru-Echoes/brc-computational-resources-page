2020: Research Allocation
https://portal.xsede.org/allocations/research#reviewcriteria


REVIEW RUBRIC 
-----------------------------------
Grounds for Rejections: 
        Failure to address either -      1) describes access to other compute resources 
                                         2) code performance and scaling data are provided
Assessment and Summary 
...


Appropriate Methodology 
…


Appropriate Research Plan 
…


Efficient use of Resources
... 


Main Document  (up to 10 pages)
-----------------------------------






Include: We have consulted with the UC Berkeley Campus Champion during the original Startup Allocation benchmarking phase and in the preparation of this Research Allocation request.
-----------------------------------


-----------------------------------


Title: descriptive title
Primary field of Science: your field
Keywords: some, key, words


Abstract:


Short description, 1 paragraph about research, 1 paragraph about compute and data requirements and how you expect to use the selected computational resource.


We have consulted with the UC Berkeley Campus Champion during the original Startup Allocation benchmarking phase and in the preparation of this Research Allocation request.


PERSONNEL / ROLES 


PI: the PI’s name
Allocation Manager: additional people who can manage the resources


PI’s CV: to be attached (NSF/NIH format, 2-page limit)




SPECIFY RESOURCES


Resource Requested: Jetstream (IU/TACC)
Resource Requested Amount: 500,000 Service Units
* Comment: short comment about how you’ll use the resource
* Virtual machines needed: 10
* Public IP addresses needed: 10


Resource Requested: Jetstream Storage
Resource Requested Amount: 3 TB
* Comment: description if large-ish request




DISCLOSURE OF ACCESS TO OTHER COMPUTE RESOURCES


The UC Berkeley campus provides a traditional HPC cluster with access to all faculty, however it does not provide the flexibility of the Jetstream cloud platform to run orchestrated docker containers for web scraping, nor Jupyter notebooks backed by docker containers.

SUPPORTING GRANTS (OPTIONAL) 


Is this project supported by grants? Yes/No


PUBLICATIONS RESULTING FROM XSEDE WORK 


None yet.




SUPPORTING DOCUMENTS 


The following documents are required (to upload):
* Main Document (Page Limit: 15)
* Code Perf & Scaling (Page Limit: 5)


Copy & paste the sections below into their own document and generate a PDF for each to be uploaded when submitting the request.
Main Document (Page Limit: 15)


We plan to use …full description of research and justification for compute and data request...


We have consulted with the UC Berkeley Campus Champion during the original Startup Allocation benchmarking phase and in the preparation of this Research Allocation request.




Code Perf & Scaling


The initial phase of the research project used the Startup Allocation with ...info about benchmarking to accurately determine requirements for this request...


We have consulted with the UC Berkeley Campus Champion during the original Startup Allocation benchmarking phase and in the preparation of this Research Allocation request.