# XSEDE Startup Allocation Request


**PI:** <THE-RESEARCHER>
**Allocation Manager:** <THE-RESEARCHER>         # PI and Allocation Manager are almost always same person (rare cases when not)


**PI’s CV:** to be attached (NSF/NIH format, 2-page limit)    # Through XSEDE User Portal 


**Title:** 
**Primary field of Science:** 


**Keywords:** 

**Resource Requested:** Jetstream (IU/TACC)                 # Example 1a
**Resource Requested Amount:** 50,000 service units         # Example 1b

**Resource Requested:** Comet (SDSC Dell Cluster with Intel Haswell Processors)         # Example 2a
**Resource Requested Amount:** 50,000 service units                                     # Example 2b

**Research Statement:**

    *1 to 2 paragraphs giving abstract of computational aspect of the researcher's work* 


**Use of Computational Resources:**

    *Couple sentences up to 1 paragraph stating what the XSEDE resources will be used to perform*

    **E.g. "Run Monte Carlo simulations..."**

**Note: please include the following shout-out to BRC (XSEDE likes when their resources are used - e.g. Campus Champion)**

This allocation request was prepared in collaboration with the UC Berkeley XSEDE Campus Champion.    

**Related Grant Information:**

*Either "Not applicable" or include NSF funding or whatever grants / funding the researcher may have.* 