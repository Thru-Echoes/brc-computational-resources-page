# brc-computational-resources-page

Revamp of https://research-it.berkeley.edu/programs/berkeley-research-computing/brc-consultants/brc-facilitated-computational-resources tables - overview of computational resources. This repo will focus primarily on XSEDE resources.

# XSEDE Resources 

## To Do 

1. Change "Scientific Gateway" to "Science Gateways" 
2. HPC@UC as a pre-XSEDE on-boarding (https://www.sdsc.edu/collaborate/hpc_at_uc.html)
3. Add ability to renew allocations (e.g. Nico renewed Startup Allocations 3 times)
4. Note that because we have Campus Champion no need to use Trial Allocation (i.e. Trial Allocation == Campus Champion Allocation)

### Trial + Campus Champion Allocations

Quickest way to obtain XSEDE resources. Turn-around times in up to **24 hours but usually within a few hours of time of adding user.** Research IT consultants are co-PIs on the UCB Campus Champion allocation giving the ability to add users directly based off of the user's XSEDE portal account username.

**Note: ** because UC Berkeley Research IT has Campus Champion there is no need to use a Trial Allocation. *Only use the Campus Champion Allocation.* 

**Steps:**

1. Have user create an XSEDE user account 
2. Add user's XSEDE username to the Campus Champion allocation 

### Startup and Education Allocations 

* 2-page allocation requests 
* only require a 1 paragraph project description 
* turn-around times in usually just a few weeks (~2-6 weeks)

Have successfully gotten over 10 Startup allocations for researchers but have never attempted an Education allocation request. 

### Science Gateways 

XSEDE Science Gateways are web portals to various compute resources such as datasets, resource hubs, modeling / simulation interfaces, etc. 

* Science Gateway application requires project have current XSEDE account
* Science Gateways require only a short paragraph description
* Science Gateways have persistent Virtual Clusters on either Bridges, Comet, or Jetstream that handle storage and job submission across those XSEDE clusters!   
* Science Gateways can make use of **XSEDE Extended Collaborative Support Services (ECSS) to "hire" XSEDE developer consultants as part-time developers for their project** *see below*

[Science Gateways Community Institute](https://sciencegateways.org/)

[XSEDE Science Gateway page](https://www.xsede.org/ecosystem/science-gateways)

Review of Science Gateways: 

"A Science Gateway is a community-developed set of tools, applications, and data that are integrated via a portal or a suite of applications, usually in a graphical user interface, that is further customized to meet the needs of a specific community. Gateways enable entire communities of users associated with a common discipline to use national resources through a common interface that is configured for optimal use. Researchers can focus on their scientific goals and less on assembling the cyberinfrastructure they require. Gateways can also foster collaborations and the exchange of ideas among researchers."

[Gateway resources](https://www.xsede.org/ecosystem/science-gateways/resources)

According to this website, Gateways support the following for reearchers: 

1. Server and web service hosting 
2. database hosting 
3. virtual clusters for persistent IPs and interface to submit jobs to clusters 

**These support features are available on Bridges, Comet, and Jetstream** 


### Extended Collaborative Support Services (ECSS) 

XSEDE has staff ECSS consultants that can be *hired* as part-time developers for a projec(t (that has an XSEDE allocation)

* ECSS request can be included with a Startup, Education, or Research Allocation request 
* ECSS request can be used with Science Gateway request to handle developing the web portal or desktop client 
* ECSS can handle developing the user auth protocols, job submission from Gateway across XSEDE resources, ...

**How to justify inclusion of ECSS for your project?**

See the following page for *questions and example answers* that PIs need to address for a successful request. 

[Successful Answers to Questions](https://www.xsede.org/web/site/for-users/ecss/ecss-justification)